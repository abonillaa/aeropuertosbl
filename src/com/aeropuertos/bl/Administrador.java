package com.aeropuertos.bl;

import java.time.LocalDate;

public class Administrador extends Persona {

    private int edad;
    private String genero;

    public Administrador(){
        super();
        this.edad = 0;
        this.genero = "";
    }

    public Administrador(String nombre, String apellidos, String identificacion, String correo, String direccion, String nacionalidad, LocalDate fechaNacimiento, int edad, String genero) {
        super(nombre, apellidos, identificacion, correo, direccion, nacionalidad, fechaNacimiento);
        this.edad = edad;
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Administrador)) return false;

        Persona persona = (Administrador) pObjeto;

        if(this.getIdentificacion().equals(persona.getIdentificacion())) return true;

        return false;
    }
}
