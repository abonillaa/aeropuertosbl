package com.aeropuertos.bl;

public class Pais {
    private String nombre;
    private String abreviatura;

    public Pais(){
        this.nombre = "";
        this.abreviatura = "";
    }

    public Pais(String nombre, String abreviatura) {
        this.nombre = nombre;
        this.abreviatura = abreviatura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Pais)) return false;

        Pais pais = (Pais) pObjeto;

        if(this.getNombre().equals(pais.getNombre())) return true;

        return false;
    }
}
