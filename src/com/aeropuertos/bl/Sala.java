package com.aeropuertos.bl;

public class Sala {

    private String nombre;
    private String codigo;
    private String ubicacion;
    private Aeropuerto aeropuerto;

    public Sala(){
        this.nombre = "";
        this.codigo = "";
        this.ubicacion = "";
        this.aeropuerto = new Aeropuerto();
    }

    public Sala(String nombre, String codigo, String ubicacion, Aeropuerto aeropuerto) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.ubicacion = ubicacion;
        this.aeropuerto = aeropuerto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Aeropuerto getAeropuerto() {
        return aeropuerto;
    }

    public void setAeropuerto(Aeropuerto aeropuerto) {
        this.aeropuerto = aeropuerto;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Sala)) return false;

        Sala sala = (Sala) pObjeto;

        if(this.getCodigo().equals(sala.getCodigo())) return true;

        return false;
    }
}
