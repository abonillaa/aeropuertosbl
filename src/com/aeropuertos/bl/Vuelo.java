package com.aeropuertos.bl;

public class Vuelo {

    private LineaArea lineaArea;
    private Aeropuerto aeropuertoSalida;
    private Aeropuerto aeropuertoDestino;
    private Pais paisSalida;
    private String horaSalida;
    private String horaLlegada;
    private String estado;
    private String tipo;
    private Aeronave aeronave;
    private Sala sala;
    private Tripulacion tripulacion;
    private int precio;

    public Vuelo(){
        this.lineaArea = new LineaArea();
        this.aeropuertoSalida = new Aeropuerto();
        this.aeropuertoDestino = new Aeropuerto();
        this.paisSalida = new Pais();
        this.horaSalida = "";
        this.horaLlegada = "";
        this.estado = "";
        this.tipo = "";
        this.aeronave = new Aeronave();
        this.sala = new Sala();
        this.tripulacion = new Tripulacion();
        this.precio = 0;
    }

    public LineaArea getLineaArea() {
        return lineaArea;
    }

    public void setLineaArea(LineaArea lineaArea) {
        this.lineaArea = lineaArea;
    }

    public Aeropuerto getAeropuertoSalida() {
        return aeropuertoSalida;
    }

    public void setAeropuertoSalida(Aeropuerto aeropuertoSalida) {
        this.aeropuertoSalida = aeropuertoSalida;
    }

    public Aeropuerto getAeropuertoDestino() {
        return aeropuertoDestino;
    }

    public void setAeropuertoDestino(Aeropuerto aeropuertoDestino) {
        this.aeropuertoDestino = aeropuertoDestino;
    }

    public Pais getPaisSalida() {
        return paisSalida;
    }

    public void setPaisSalida(Pais paisSalida) {
        this.paisSalida = paisSalida;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Aeronave getAeronave() {
        return aeronave;
    }

    public void setAeronave(Aeronave aeronave) {
        this.aeronave = aeronave;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Tripulacion getTripulacion() {
        return tripulacion;
    }

    public void setTripulacion(Tripulacion tripulacion) {
        this.tripulacion = tripulacion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Vuelo)) return false;

        Vuelo vuelo = (Vuelo) pObjeto;

        if(this.getAeronave().equals(vuelo.getAeronave()) && this.getLineaArea() == vuelo.getLineaArea()) return true;

        return false;
    }
}
