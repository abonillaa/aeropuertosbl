package com.aeropuertos.bl;

public class LineaArea {

    private String nombreComercial;
    private String cedulaJuridica;
    private String empresa;
    private Pais pais;
    private String logo;

    public LineaArea(){
        this.nombreComercial = "";
        this.cedulaJuridica = "";
        this.empresa = "";
        this.pais = new Pais();
        this.logo = "";
    }

    public LineaArea(String nombreComercial, String cedulaJuridica, String empresa, Pais pais, String logo) {
        this.nombreComercial = nombreComercial;
        this.cedulaJuridica = cedulaJuridica;
        this.empresa = empresa;
        this.pais = pais;
        this.logo = logo;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getCedulaJuridica() {
        return cedulaJuridica;
    }

    public void setCedulaJuridica(String cedulaJuridica) {
        this.cedulaJuridica = cedulaJuridica;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof LineaArea)) return false;

        LineaArea lineaArea = (LineaArea) pObjeto;

        if(this.getCedulaJuridica().equals(lineaArea.getCedulaJuridica())) return true;

        return false;
    }
}
