package com.aeropuertos.bl;

public class Aeropuerto {

    private String nombre;
    private String codigo;
    private Pais pais;
    private String codigoPais;
    private Administrador administrador;

    public Aeropuerto(){
        this.nombre = "";
        this.codigo = "";
        this.pais = new Pais();
        this.codigoPais = "";
        this.administrador = new Administrador();
    }

    public Aeropuerto(String nombre, String codigo, Pais pais, String codigoPais, Administrador administrador) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.pais = pais;
        this.codigoPais = codigoPais;
        this.administrador = administrador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Aeropuerto)) return false;

        Aeropuerto aeropuerto = (Aeropuerto) pObjeto;

        if(this.getCodigo().equals(aeropuerto.getCodigo())) return true;

        return false;
    }
}
