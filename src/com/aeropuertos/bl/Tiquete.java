package com.aeropuertos.bl;

import java.time.LocalDate;

public class Tiquete {

    private int numeroAsiento;
    private LocalDate fechaSalida;
    private String codigoAeropuertoSalida;
    private String codigoAeropuertoDestino;
    private String codigoPaisSalida;
    private String codigoPaisDestino;
    private int precio;
    private int numeroVuelo;
    private String tipoAsiento;
    private String identificacionUsuario;


    public Tiquete(){
        this.numeroAsiento = 0;
        this.fechaSalida = LocalDate.now();
        this.codigoAeropuertoSalida = "";
        this.codigoAeropuertoDestino = "";
        this.codigoPaisSalida = "";
        this.codigoPaisDestino = "";
        this.precio = 0;
        this.numeroVuelo = 0;
        this.tipoAsiento = "";
        this.identificacionUsuario = "";
    }

    public Tiquete(int numeroAsiento, LocalDate fechaSalida, String codigoAeropuertoSalida, String codigoAeropuertoDestino, String codigoPaisSalida, String codigoPaisDestino, int precio, int numeroVuelo, String tipoAsiento, String identificacionUsuario) {
        this.numeroAsiento = numeroAsiento;
        this.fechaSalida = fechaSalida;
        this.codigoAeropuertoSalida = codigoAeropuertoSalida;
        this.codigoAeropuertoDestino = codigoAeropuertoDestino;
        this.codigoPaisSalida = codigoPaisSalida;
        this.codigoPaisDestino = codigoPaisDestino;
        this.precio = precio;
        this.numeroVuelo = numeroVuelo;
        this.tipoAsiento = tipoAsiento;
        this.identificacionUsuario = identificacionUsuario;
    }

    public int getNumeroAsiento() {
        return numeroAsiento;
    }

    public void setNumeroAsiento(int numeroAsiento) {
        this.numeroAsiento = numeroAsiento;
    }

    public LocalDate getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(LocalDate fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getCodigoAeropuertoSalida() {
        return codigoAeropuertoSalida;
    }

    public void setCodigoAeropuertoSalida(String codigoAeropuertoSalida) {
        this.codigoAeropuertoSalida = codigoAeropuertoSalida;
    }

    public String getCodigoAeropuertoDestino() {
        return codigoAeropuertoDestino;
    }

    public void setCodigoAeropuertoDestino(String codigoAeropuertoDestino) {
        this.codigoAeropuertoDestino = codigoAeropuertoDestino;
    }

    public String getCodigoPaisSalida() {
        return codigoPaisSalida;
    }

    public void setCodigoPaisSalida(String codigoPaisSalida) {
        this.codigoPaisSalida = codigoPaisSalida;
    }

    public String getCodigoPaisDestino() {
        return codigoPaisDestino;
    }

    public void setCodigoPaisDestino(String codigoPaisDestino) {
        this.codigoPaisDestino = codigoPaisDestino;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getNumeroVuelo() {
        return numeroVuelo;
    }

    public void setNumeroVuelo(int numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public String getTipoAsiento() {
        return tipoAsiento;
    }

    public void setTipoAsiento(String tipoAsiento) {
        this.tipoAsiento = tipoAsiento;
    }

    public String getIdentificacionUsuario() {
        return identificacionUsuario;
    }

    public void setIdentificacionUsuario(String identificacionUsuario) {
        this.identificacionUsuario = identificacionUsuario;
    }

    public boolean equals(Object pObjeto){
        if(this == pObjeto) return true;

        if(pObjeto == null) return false;

        if(!(pObjeto instanceof Tiquete)) return false;

        Tiquete tiquete = (Tiquete) pObjeto;

        if(this.getIdentificacionUsuario().equals(tiquete.getIdentificacionUsuario()) && this.getNumeroVuelo() == tiquete.getNumeroVuelo()) return true;

        return false;
    }
}
